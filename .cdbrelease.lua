-- LUALOCALS < ---------------------------------------------------------
local dofile
    = dofile
-- LUALOCALS > ---------------------------------------------------------

return {
	user = "Warr1024",
	pkg = "nodecore",
	min = "5.0",
	version = dofile("./mods/nc_api/version.lua")
}
