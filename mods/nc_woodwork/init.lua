-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("adze")
include("plank")
include("staff")
include("tools")
include("ladder")
include("shelf")
