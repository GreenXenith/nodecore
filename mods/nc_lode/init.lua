-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include("ore")
include("metallurgy")
include("tools")
include("shafts")
include("shelf")
