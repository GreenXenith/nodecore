-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs
    = minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local footsteps = {}

minetest.register_on_joinplayer(function(player)
		local inv = player:get_inventory()
		inv:set_size("main", 8)
		inv:set_size("craft", 0)
		inv:set_size("craftpreview", 0)
		inv:set_size("craftresult", 0)

		player:set_physics_override({speed = 1.25})

		player:set_properties({
				makes_footstep_sound = true,

				-- Allow slight zoom for screenshots
				zoom_fov = 60
			})
		footsteps[player:get_player_name()] = true
	end)

minetest.register_allow_player_inventory_action(function(_, action)
		return action == "move" and 0 or 1000000
	end)

minetest.unregister_chatcommand("kill")

minetest.register_globalstep(function()
		for _, player in pairs(minetest.get_connected_players()) do
			local name = player:get_player_name()
			local value = not player:get_player_control().sneak
			if footsteps[name] ~= value then
				player:set_properties({
						makes_footstep_sound = value
					})
				footsteps[name] = value
			end
		end
	end)
