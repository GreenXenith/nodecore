-- LUALOCALS < ---------------------------------------------------------
local include, nodecore
    = include, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.amcoremod()

include('api')
include('node')
include('biome')
include('strata')
include('ore')
include('grasslife')
include('ambiance')
